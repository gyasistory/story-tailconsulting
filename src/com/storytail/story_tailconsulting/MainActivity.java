package com.storytail.story_tailconsulting;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.parse.Parse;
import com.parse.ParseObject;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		Parse.initialize(this, "OO3iD238JgvbV2lwU0N1SqqQAyXA3n3wYsApkaBB", "92AZQBYYR2vBzGvgd4qKeidLf6Etze9hYjxDtaxG");
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		//  This is a test
		ParseObject testObject = new ParseObject("TestObject");
		testObject.put("foo", "bar");
		testObject.saveInBackground();
		
		//  This is just to test the button
		Button b = (Button) findViewById(R.id.aboutButton);
		b.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// Start Service Activity
				startActivity(new Intent(MainActivity.this, ServiceListActivity.class));
				
			}
		});
	}

	
	

}
